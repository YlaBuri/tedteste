package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Barco;
import model.Tabuleiro;
import view.ViewTabuleiro;


public class Controller implements ActionListener, KeyListener{
	ViewTabuleiro viewTabuleiro = new ViewTabuleiro();
	static Tabuleiro tabuleiro = new Tabuleiro();
	public Controller(ViewTabuleiro viewTabuleiro) throws IOException {
        this.viewTabuleiro = viewTabuleiro;
        this.viewTabuleiro.btnAtacar.addActionListener(this);
        adicionarBarcos();
        preencherTabela(this.viewTabuleiro.jtTabuleiro);
        
    }
    
    public void preencherTabela(JTable tabela) throws IOException{
        DefaultTableModel JTtabuleiro= new DefaultTableModel();
        tabela.setModel(JTtabuleiro);
              
        JTtabuleiro.addColumn("A");
        JTtabuleiro.addColumn("B");
        JTtabuleiro.addColumn("C");
        JTtabuleiro.addColumn("D");
        JTtabuleiro.addColumn("E");
        JTtabuleiro.addColumn("F");
        JTtabuleiro.addColumn("G");
        JTtabuleiro.addColumn("H");
        JTtabuleiro.addColumn("I");
        JTtabuleiro.addColumn("J");
               
        	
		Object [] coluna  = new Object[10];
		for(int i = 0; i<10; i++){
            for (int j = 0; j < 10; j++) {
               coluna[j] = tabuleiro.getCampos()[i][j];        
            } 
            JTtabuleiro.addRow(coluna); 
        }
    }
    public void adicionarBarcos() throws IOException{
        Barco barco1 = tabuleiro.criarBarco(3, true);
		Barco barco2 = tabuleiro.criarBarco(4, false);
		Barco barco3 = tabuleiro.criarBarco(5, true);
		tabuleiro.adicionarBarcoNoTabuleiro(0, 3, barco1);
		tabuleiro.adicionarBarcoNoTabuleiro(2, 7, barco2);
		tabuleiro.adicionarBarcoNoTabuleiro(4, 0, barco3);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int linha= viewTabuleiro.jtTabuleiro.getSelectedRow();
        int coluna=viewTabuleiro.jtTabuleiro.getSelectedColumn();
        if(e.getSource()== viewTabuleiro.btnAtacar){
            //JOptionPane.showMessageDialog(viewTabuleiro, "linha:"+ linha+"\ncoluna: "+coluna+"= "+ tabuleiro.getCampos()[linha][coluna]);
            tabuleiro.atirar(linha, coluna);
            try {
				preencherTabela(viewTabuleiro.jtTabuleiro);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        }
    }

	@Override
	public void keyPressed(KeyEvent k) {
		
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
