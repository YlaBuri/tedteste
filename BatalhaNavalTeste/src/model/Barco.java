package model;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Barco {
	private Image imagem;
	private Integer tamanho;
	private boolean vivo;
	private boolean posicao;
	private Integer vidas;

	public Barco(Integer tamanho, boolean posicao) throws IOException {
		super();
		this.tamanho = tamanho;
		this.vivo = true;
		this.posicao = posicao;
		this.vidas = tamanho;
		imagem = ImageIO.read(new File("Imagens/nave.png"));
	}

	public boolean getVivo() {
		return vivo;
	}

	public void setVivo(boolean vivo) {
		this.vivo = vivo;
	}

	public Integer getVidas() {
		return vidas;
	}

	public void setVidas(Integer vidas) {
		this.vidas = vidas;
	}

	public Integer getTamanho() {
		return tamanho;
	}

	public boolean getPosicao() {
		return posicao;
	}

}
