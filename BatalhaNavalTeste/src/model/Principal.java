package model;

import java.io.IOException;

public class Principal {

	public static void main(String[] args) throws IOException {
		Tabuleiro tabuleiro = new Tabuleiro();
		Barco barco1 = tabuleiro.criarBarco(3, true);
		Barco barco2 = tabuleiro.criarBarco(4, false);
		Barco barco3 = tabuleiro.criarBarco(5, true);

		tabuleiro.imprimirTabuleiro();
		System.out.println();
		tabuleiro.adicionarBarcoNoTabuleiro(0, 3, barco1);
		tabuleiro.adicionarBarcoNoTabuleiro(2, 7, barco2);
		tabuleiro.adicionarBarcoNoTabuleiro(4, 0, barco3);

		tabuleiro.imprimirTabuleiro();
		tabuleiro.atirar(0, 4);
		System.out.println();
		tabuleiro.imprimirTabuleiro();

	}

}
