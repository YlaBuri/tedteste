package model;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class Tabuleiro {
	private String[][] campos;
	private List<Barco> barcos = new ArrayList<Barco>();

	public Tabuleiro() {
		campos = new String[10][10];
		for (int i = 0; i < campos.length; i++) {
			for (int j = 0; j < campos.length; j++) {
				campos[i][j] = "~";
			}
		}
	}

	public Barco criarBarco(int tamanho, boolean posicao) throws IOException {
		Barco barco = new Barco(tamanho, posicao);
		barcos.add(barco);
		return barco;
	}

	public void adicionarBarcoNoTabuleiro(int i, int j, Barco barco) {
		for (int k = 0; k < barco.getTamanho(); k++) {
			if (barco.getPosicao() == true) {
				campos[i][j] = "#";
				j++;
			} else {
				campos[i][j] = "#";
				i++;
			}
		}
	}

	public void imprimirTabuleiro() {
		for (int i = 0; i < campos.length; i++) {
			for (int j = 0; j < campos.length; j++) {
				System.out.print(campos[i][j] + " ");
			}
			System.out.println();
		}
	}

	public void atirar(int i, int j) {
		if (campos[i][j].equalsIgnoreCase("#")) {
			JOptionPane.showMessageDialog(null, "Acertou um tiro");
			campos[i][j] = "x";
		} else {
			JOptionPane.showMessageDialog(null, "Errou o tiro");
		}
	}

	public void afundarBarco() {

	}

	public String[][] getCampos() {
		return campos;
	}
	
}
