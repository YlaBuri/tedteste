package principal;

import java.io.IOException;

import view.ViewTabuleiro;
import controller.Controller;

public class BatalhaNaval {

	public static void main(String[] args) throws IOException {
		ViewTabuleiro vt = new ViewTabuleiro();
        Controller controller = new Controller(vt);
        vt.setVisible(true);
        vt.setLocationRelativeTo(null);

	}

}
